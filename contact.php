<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/fav-icon.png">
  <!-- Author Meta -->
  <meta name="author" content="">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Magpie Audio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS Files -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
  <!-- Main Stylesheet File -->
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css">
  <!-- JS Files -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   <script src="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js"></script>
   <script src="js/main.js"></script>

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="58">
  <!--Header Starts here-->
  <header id="header">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-product-page">
      <a class="navbar-brand" href="index.php"><img class="logo-size" src="img/logo.png" alt="logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="amplifier-product.php">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="#">Success Stories</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="#">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product active" href="#">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>


  <section id="contact-landing">
    <h2 class="font-top-product">GET IN TOUCH</h2>
  </section>


<form id="contact-page">
  <div class="container">
    <h2 class="contact-heading">Drop a Message</h2>
    <p class="contact-para">Lorem Ipsum is simply dummy text of the printing and typesetting</p>
    <p></p>
  <div class="row pd-contact-top">
    <div class="col-md-6">
      <label class="label-contact" for="formGroupExampleInput">Your Name*</label>
      <input type="text" class="form-control form-bottom-line" placeholder="Enter Your Name" required>
    </div>
    <div class="col-md-6">
      <label class="label-contact" for="formGroupExampleInput">Email*</label>
      <input type="email" class="form-control form-bottom-line" placeholder="Enter Your Email Address" required>
    </div>
  </div>
  <div class="row pd-contact-top">
    <div class="col-md-6">
      <label class="label-contact" for="formGroupExampleInput">Phone*</label>
      <input type="text" class="form-control form-bottom-line" placeholder="Enter Your Phone Number" required>
    </div>
    <div class="col-md-6">
      <label class="label-contact" for="formGroupExampleInput">Company*</label>
      <input type="text" class="form-control form-bottom-line" placeholder="Enter Your Company" required>
    </div>
  </div>
  <div class="form-group pd-contact-top">
    <label class="label-contact" for="exampleFormControlInput1">Message*</label>
    <input type="text" class="form-control form-bottom-line-mssg" id="exampleFormControlInput1" placeholder="" required>
  </div>
  <div class="bttn-align">
  <button type="submit" class="btn btn-primary contact-page-bttn">Send Message</button>
  </div>
  </div>
</form>



<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497698.66007256677!2d77.35073295903771!3d12.954517012303143!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C%20Karnataka!5e0!3m2!1sen!2sin!4v1590056348792!5m2!1sen!2sin" width="100%" height="500" frameborder="0" style="border:0;margin-bottom:-5px;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>




<section id="contact">
  <div class="container footer-page-links">
    <div class="row">
      <div class="col-md-3">
        <h5 class="contact-sub-heading">CONTACT US</h5>

        <div class="addressholder">
        <ul>
        <li>
        <i><img class="img-fluid" src="img/location.png" alt="Contact"></i>
        <span class="ctnaddr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</span>
        </li>
        <li>
        <i><img class="img-fluid" src="img/phone.png" alt="Contact"></i>
        <span class="ctnaddr">+91 98453-03199
        </span>
        </li>
        <li>
        <i><img class="img-fluid" src="img/mail.png" alt="Contact"></i>
        <span class="ctnaddr">prasad@awan.co.in</span>
        </li>
        </ul>
        </div>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">QUICK LINKS</h5>
        <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">Products</a>
              </li>
              <li>
                <a href="#">Success Stories</a>
              </li>
              <li>
                <a href="#">Blog</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">EXTRAS</h5>
        <ul>
              <li>
                <a href="#">Downloads</a>
              </li>
              <li>
                <a href="#">News & Events</a>
              </li>
              <li>
                <a href="#">Privacy Policy</a>
              </li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">NEWSLETTER</h5>
        <p>Give us your email and you will be daily updated with the latest events, in detail.</p>
        <div>
        <input class="news-input" type="email" name="email" placeholder="Enter Your Email Address"><img class="email-contact-img" src="img/newsletter-icon.png">
        </div>
        <h5 class="contact-sub-heading">FOLLOW US</h5>
        <p><img class="social-img" src="img/fb.png"><img class="social-img" src="img/twitter.png"><img class="social-img" src="img/whatsapp.png"><img class="social-img" src="img/insta.png"></p>

      </div>
    </div>
  </div>
  
</section>
<footer>
<div class="container copyright">
      <div class="row align-items-center">
        <div class="col-sm-3">
          <a href="index.php">
            <a href="#">Terms and Conditions</a>
          </a>
        </div>
        <div class="col-sm-6">
          © 2020. Magpie. All Rights Reserved.
        </div>
        <div class="col-sm-3">
          <a href="#">Powered by ViaMagus</a>
        </div>
      </div>
    </div>
</footer>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title" id="exampleModalLabel">Get a Quote</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" required="" name="name" class="form-control" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email1" placeholder="Email">
          </div> 
          <div class="form-group">
            <input type="text" required="" name="subject" class="form-control" placeholder="Subject">
          </div>
          <div class="form-group">
          <textarea class="form-control" rows="5" id="comment" placeholder="Message"></textarea>
        </div>      
        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-center">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
 <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script>
  $().ready(function(){
  $('.slick-carousel').slick({
    arrows: true,
    centerPadding: "0px",
    dots: false,
    slidesToShow: 3,
    infinite: false,
    easing:"linear",
     responsive:[
      {breakpoint:767,settings:{
        slidesToShow:2,
      }},
      {breakpoint:500,settings:{
        slidesToShow:1,
      }},
    ],
  });
});
</script>

</body>



</html>