
(function ($) {
  "use strict";

  // Porfolio isotope and filter
  $(window).on('load', function () {
    var portfolioIsotope = $('.portfolio-container').isotope({
      itemSelector: '.portfolio-item'
    });
    $('#portfolio-flters li').on( 'click', function() {
      $("#portfolio-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');
 
      portfolioIsotope.isotope({ filter: $(this).data('filter') });
    });
  });


  
 $(document).ready(function(){
 	
   $('.nav-link').click(function(){
    var divId = $(this).attr('href');
     $('html, body').animate({
      scrollTop: $(divId).offset().top - 55
    }, 500);
  });
 });
 $(document).on('click','.navbar-collapse.show', function(e) {
    if( $(e.target).is('a') ) {
        $(this).collapse('hide');
    }
});
 $(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll > 50) {
    $('.navbar').addClass('navbar-black');
  }
  else {
    $('.navbar').removeClass('navbar-black');
  }
});

    // Porfolio isotope and filter
  $(window).on('load', function () {
    var portfolioproductIsotope = $('.portfolio-product-container').isotope({
      itemSelector: '.portfolio-product-item',
      filter:'.events'
    });
    $('#portfolio-product-flters li').on( 'click', function() {
      $("#portfolio-product-flters li").removeClass('filter-active');
      $(this).addClass('filter-active');
  
      portfolioproductIsotope.isotope({ filter: $(this).data('filter') });
    });
  }); 


})(jQuery);

