<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/fav-icon.png">
  <!-- Author Meta -->
  <meta name="author" content="">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Magpie Audio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS Files -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
  <!-- Main Stylesheet File -->
  <link rel="stylesheet" href="css/main.css">
  <!-- JS Files -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   <script src="js/main.js"></script>

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="58">
  <!--Header Starts here-->
  <header id="header">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top">
      <a class="navbar-brand" href="index.php"><img class="logo-size" src="img/logo.png" alt="logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link active" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="amplifier-product.php">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Success Stories</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="contact.php">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>


  <section id="landing">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="landing-img-text">
      <img class="d-block w-100 img-fluid" src="img/landing-bg-amplifiers.jpg" alt="First slide">
      <div class="text-on-img">
    <h2 class="landing-img-text1">Mix it Up!</h2>
    <p class="landing-img-text2">WITH POWER AMPLIFIERS</p>
    </div>
    <div class="landing-prod-img">
      <img class="landing-product-align img-fluid" src="img/amplifiers.png">
    </div>
  </div>
    </div>
    <div class="carousel-item">
      <div class="landing-img-text">
      <img class="d-block w-100 img-fluid" src="img/landing-bg-infrared-wireless.jpg" alt="Second slide">
      <div class="text-on-img">
    <h2 class="landing-img-text1">Lorem Ipsum!</h2>
    <p class="landing-img-text2">WITH INFRARED WIRELESS</p>
    </div>
    <div class="landing-prod-img">
      <img class="landing-product-align img-fluid" src="img/infrared.png">
    </div>
  </div>
    </div>
    <div class="carousel-item">
      <div class="landing-img-text">
      <img class="d-block w-100 img-fluid" src="img/landing-bg-loudspeakers.jpg" alt="Third slide">
      <div class="text-on-img">
    <h2 class="landing-img-text1">Dolor sit Amet!</h2>
    <p class="landing-img-text2">WITH TWO WAY SPEAKER</p>
    </div>
    <div class="landing-prod-img">
      <img class="landing-product-align img-fluid" src="img/speakers.png">
    </div>
  </div>
    </div>
    <div class="carousel-item">
      <div class="landing-img-text">
      <img class="d-block w-100 img-fluid" src="img/landing-bg-podium.jpg" alt="Third slide">
      <div class="text-on-img">
    <h2 class="landing-img-text1">Lorem Ipsum!</h2>
    <p class="landing-img-text2">WITH PODIUM</p>
    </div>
    <div class="landing-prod-img">
      <img class="landing-product-align img-fluid" src="img/podium.png">
    </div>
  </div>
    </div>
    <div class="carousel-item">
      <div class="landing-img-text">
      <img class="d-block w-100 img-fluid" src="img/landing-bg-stage-monitors.jpg" alt="Third slide">
      <div class="text-on-img">
    <h2 class="landing-img-text1">Dolor sit Amet!</h2>
    <p class="landing-img-text2">WITH STAGE MONITORS</p>
    </div>
    <div class="landing-prod-img">
      <img class="landing-product-align img-fluid" src="img/stage-monitors.png">
    </div>
  </div>
    </div>
  </div>
  <div class="carousel-controls">
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <img src="./img/prev.png" alt="Left Arrow">
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <img src="./img/next.png" alt="Right Arrow">
    <span class="sr-only">Next</span>
  </a>
  </div>
</div>
  </section>

<section id="about">
  <div class="container">
  <div class="row">
    <div class="col-md-7 my-auto about-text-padding">
      <h2 class="about-heading1">We Provide</h2>
      <h2 class="about-heading2">The Best Audio Equipments.</h2>
      <p class="about-para">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. Remaining essentially unchanged.</p>
    </div>
    <div class="col-md-4">
      <img class="about-img img-fluid" src="img/about-img.png">
    </div>
  </div>
  </div>
 <img class="img-bt-left img-fluid" src="img/side-top.png">
</section>



<section id="portfolio"  class="clearfix" >
      <div class="container">
        
        <div class="row">
          
        <div class="col-md-3">
          
      
        <div class="row">
          <div class="col-md-12 pl-2 products-right-line">
            <h2 class="heading2-services">Featured <span class="heading3-services"><br>Products</span></h2>
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li><br>
              <li data-filter=".power-amplifier">AMPLIFIERS</li><br>
              <li data-filter=".infrared-wireless">INFRARED WIRELESS</li><br>
              <li data-filter=".two-way-speaker">SPEAKERS</li><br>
              <li data-filter=".product-name">PODIUMS</li><br>
              <li data-filter=".magpie-lectren">POINT SOURCES</li><br>
              <li data-filter=".big-speaker">STAGE MONITORS</li>
            </ul>
          </div>
        </div>
  </div>
  <div class="col-md-9 br-left-col">
        <div class="row portfolio-container">

        <!-- EVENTS & EXHIBITIONS -->

        <div class="col-md-4 portfolio-item power-amplifier">
               <div class="card">
  <img class="card-img-top" src="img/product-1.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">POWER AMPLIFIER</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
    <p class="text-center"> <a href="amplifier-product.php" class="card-link">View Details</a></p>
  </div>
</div>
        </div> 
<!-- 
        <div class="col-md-4 portfolio-item power-amplifier">
               <div class="card">
  <img class="card-img-top" src="img/product-1.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">MIXED AMPLIFIER</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
    <p class="text-center"> <a href="amplifier-product.php" class="card-link">View Details</a></p>
  </div>
</div>
        </div>        
 -->

        <div class="col-md-4 portfolio-item infrared-wireless">
               <div class="card">
  <img class="card-img-top" src="img/product-2.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">INFRARED WIRELESS</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
   <p class="text-center"> <a href="#" class="card-link">View Details</a></p>
  </div>
</div>
        </div>        



        <div class="col-md-4 portfolio-item two-way-speaker">
               <div class="card">
  <img class="card-img-top" src="img/product-3.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">TWO WAY SPEAKER</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
   <p class="text-center"> <a href="#" class="card-link">View Details</a></p>
  </div>
</div>
        </div>          



        <div class="col-md-4 portfolio-item product-name">
               <div class="card">
  <img class="card-img-top" src="img/product-5.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">DIGITAL PODIUMS</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
    <p class="text-center"><a href="#" class="card-link">View Details</a><p>
  </div>
</div>
        </div>       


        <div class="col-md-4 portfolio-item magpie-lectren">
               <div class="card">
  <img class="card-img-top" src="img/product-4.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">POINT SOURCES</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
   <p class="text-center"> <a href="#" class="card-link">View Details</a><p>
  </div>
</div>
        </div>       


        <div class="col-md-4 portfolio-item big-speaker">
               <div class="card">
  <img class="card-img-top" src="img/product-6.png" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">STAGE MONITORS</h5>
    <p class="card-text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
    <p class="text-center"><a href="#" class="card-link">View Details</a><p>
  </div>
</div>
        </div> 
</div>
</div>
        </div>

      </div>
      <img class="img-top-left img-fluid" src="img/side-bottom.png">
      <img class="img-bottom-right img-fluid" src="img/side-bottom-right.png">
    </section>



<section id="success-stories">
  <img class="img-top-right img-fluid" src="img/side-top-right.png">
  <div class="container">
     <h2 class="success-stories-heading1">SUCCESS STORIES</h2>
  <div class="row pt-5"> 
    <div class="col-md-12">
     <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row">
        <div class="col-md-2"></div>
      <div class="col-md-4">
      <img class="d-block w-100" src="img/success.png" alt="First slide">
      </div>
      <div class="col-md-4 my-auto">
     <p class="success-para">"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"</p>
     <p><a href="">Read More</a></p>
      </div>
      <div class="col-md-2"></div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="row">
        <div class="col-md-2"></div>
      <div class="col-md-4">
      <img class="d-block w-100" src="img/success.png" alt="First slide">
      </div>
      <div class="col-md-4 my-auto">
     <p class="success-para">" Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s "</p>
     <p><a href="">Read More</a></p>
      </div>
      <div class="col-md-2"></div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="row">
        <div class="col-md-2"></div>
      <div class="col-md-4">
      <img class="d-block w-100" src="img/success.png" alt="First slide">
      </div>
      <div class="col-md-4 my-auto">
     <p class="success-para">" Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s "</p>
     <p><a href="">Read More</a></p>
      </div>
      <div class="col-md-2"></div>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </div>
  </div>
  </div>
</section>


<section id="blog">
  <img class="img-top-left img-fluid" src="img/side-bottom.png">
  <div class="container">
     <h2 class="success-stories-heading1">Blog</h2>
     <p class="blog-para"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
  <div class="row pt-5"> 
    <div class="col-md-12">
     <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselIndicators" data-slide-to="1"></li>
    <li data-target="#carouselIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row">
        <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img1.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
      <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img2.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
      <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img3.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>



    </div>
  </div>
    <div class="carousel-item">
      <div class="row">
        
        <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img1.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
      <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img2.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
      <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img3.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
    </div>
    </div>
     <div class="carousel-item">
      <div class="row">
        
        <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img1.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
      <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img2.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
      <div class="col-md-4">
           <img class="blog-img-top img-fluid" src="img/blog-img3.jpg" alt="image">
           <p class="blog-time"><i class="fa fa-clock-o"></i>MARCH 3, 2020</p>
         <h3 class="blog-title">Blog Sample Post</h3>
          <p class="blog-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
         <a href="#" class="blog-link">Read More</a>
      </div>
    </div>
    </div>
    
  </div>
  
</div>
    </div>
  </div>
  </div>
  <img class="img-bottom-right img-fluid" src="img/side-bottom-right.png">
</section>



<section id="contact">
  <div class="container footer-page-links">
    <div class="row">
      <div class="col-md-3">
        <h5 class="contact-sub-heading">CONTACT US</h5>

        <div class="addressholder">
        <ul>
        <li>
        <i><img class="img-fluid" src="img/location.png" alt="Contact"></i>
        <span class="ctnaddr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</span>
        </li>
        <li>
        <i><img class="img-fluid" src="img/phone.png" alt="Contact"></i>
        <span class="ctnaddr">+91 98453-03199
        </span>
        </li>
        <li>
        <i><img class="img-fluid" src="img/mail.png" alt="Contact"></i>
        <span class="ctnaddr">prasad@awan.co.in</span>
        </li>
        </ul>
        </div>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">QUICK LINKS</h5>
        <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">Products</a>
              </li>
              <li>
                <a href="#">Success Stories</a>
              </li>
              <li>
                <a href="#">Blog</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
        </ul>
      </div>
      <div class="col-md-2">
        <h5 class="contact-sub-heading">EXTRAS</h5>
        <ul>
              <li>
                <a href="#">Downloads</a>
              </li>
              <li>
                <a href="#">News & Events</a>
              </li>
              <li>
                <a href="#">Privacy Policy</a>
              </li>
        </ul>
      </div>
      <div class="col-md-4">
        <h5 class="contact-sub-heading">NEWSLETTER</h5>
        <p>Give us your email and you will be daily updated with the latest events, in detail.</p>
        <div>
        <input class="news-input place-holder-font" type="email" name="email" placeholder="Enter Your Email Address"><img class="email-contact-img" src="img/newsletter-icon.png">
        </div>
        <h5 class="contact-sub-heading">FOLLOW US</h5>
        <p><img class="social-img" src="img/fb.png"><img class="social-img" src="img/twitter.png"><img class="social-img" src="img/whatsapp.png"><img class="social-img" src="img/insta.png"></p>

      </div>
    </div>
  </div>
  
</section>
<footer>
<div class="container copyright">
      <div class="row align-items-center">
        <div class="col-sm-3">
          <a href="index.php">
            <a href="#">Terms and Conditions</a>
          </a>
        </div>
        <div class="col-sm-6">
          © 2020. Magpie. All Rights Reserved.
        </div>
        <div class="col-sm-3">
          <a href="#">Powered by ViaMagus</a>
        </div>
      </div>
    </div>
</footer>
<div class="talk-to-us-fixed">
    <a href="talk-to-us.php" class="talk-to-us" data-toggle="modal" data-target="#form">Get a Quote</a>
</div>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title" id="exampleModalLabel">Get a Quote</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" required="" name="name" class="form-control" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email1" placeholder="Email">
          </div> 
          <div class="form-group">
            <input type="text" required="" name="subject" class="form-control" placeholder="Subject">
          </div>
          <div class="form-group">
          <textarea class="form-control" rows="5" id="comment" placeholder="Message"></textarea>
        </div>      
        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-center">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
 <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
</body>

</html>