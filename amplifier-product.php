<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Mobile Specific Meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Favicon-->
  <link rel="shortcut icon" href="img/fav-icon.png">
  <!-- Author Meta -->
  <meta name="author" content="">
  <!-- Meta Description -->
  <meta name="description" content="">
  <!-- Meta Keyword -->
  <meta name="keywords" content="">
  <!-- meta character set -->
  <meta charset="UTF-8">
  <!-- Site Title -->
  <title>Magpie Audio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSS Files -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap" rel="stylesheet">
  <!-- Main Stylesheet File -->
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css">
  <!-- JS Files -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-migrate-3.0.1.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
   <script src="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js"></script>
   <script src="js/main.js"></script>

</head>

<body data-spy="scroll" data-target=".navbar" data-offset="58">
  <!--Header Starts here-->
  <header id="header">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-product-page">
      <a class="navbar-brand" href="index.php"><img class="logo-size" src="img/logo.png" alt="logo"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product active" href="#">Products</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="#">Success Stories</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="#">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link-product" href="contact.php">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>


  <section id="product-landing">
    <h2 class="font-top-product">POWER AMPLIFIER</h2>
  </section>



<section id="amplifier">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="row">
          <div class="col-md-12">
            <img class="product-left-img" src="img/product-1.png">
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
      <section id="portfolio-product"  class="clearfix" >
      <div class="container">
        <div class="row">
          <div class="col-md-12 p-0">
            <ul id="portfolio-product-flters">
              <li data-filter=".events" class="filter-active">Description</li>
              <li data-filter=".activities">Specification</li>
              <li data-filter=".printing">Downloads</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-product-container">

        <!-- EVENTS & EXHIBITIONS -->

        <div class="col-md-12 portfolio-product-item events">
               <div class="portfolio-product-wrap">
                <p class="description-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                <p class="description-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...</p>
                <h4 class="description-sub-heading">Key Features</h4>
                <p class="description-para2">4 channel amplifier<br>
                2800 W per channel at 4 ohms<br>
                Versatile, compact and highly energy-efficient<br>
                Optional Dante networked audio</p>
               </div>
        </div>        

       <!-- ON GROUND ACTIVATIONS  -->

       <div class="col-md-4 portfolio-product-item activities">
               <div class="portfolio-product-wrap">
                  <a href="img/OnGround-1.jpg" data-toggle="lightbox" data-gallery="gallery">
                  <img src="img/OnGround-1.jpg" class="img-fluid" alt="">
                </a>
               </div>
        </div>


       <!-- PRINTING & MERCHANDISING   -->

       <div class="col-md-4 portfolio-product-item printing">
               <div class="portfolio-product-wrap">
                  <a href="img/Merchandising-1.jpg" data-toggle="lightbox" data-gallery="gallery">
                  <img src="img/Merchandising-1.jpg" class="img-fluid" alt="">
                </a>
               </div>
        </div>
        <div class="col-md-4 portfolio-product-item printing">
               <div class="portfolio-product-wrap">
                  <a href="img/Merchandising-2.jpg" data-toggle="lightbox" data-gallery="gallery">
                  <img src="img/Merchandising-2.jpg" class="img-fluid" alt="">
                </a>
               </div>
        </div>

     

        </div>

      </div>
    </section>
          </div>
        </div>
      </div>
      <div class="col-md-5">
        <div class="row">
          <div class="col-md-12">
            <h2 class="amplifier-heading">Amplifiers</h2>
            <h2 class="amplifier-sub-heading">MODEL NUMBER - MP 252</h2>
            <P>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took</P>
            <a class="product-more-link" href="">Read all products reviews here</a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h2 class="amplifier-heading2">Applications</h2>
            <p>House of worship<br>
            Theatres, auditoria and performing arts centers<br>
            Live music clubs<br>
            Corporate A/V</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <h2 class="amplifier-heading3">Product Video</h2>
            <img class="product-left-img" src="img/video-img.png">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<section id="botom-carousel">
  <h2 class="bottom-slider-heading">Related Products</h2>
<div class="slick-carousel">
  <div><div class="slide-content"><img class="bottom-slider-img" src="img/amplifier-product-img1.png">
  MODEL NUMBER - MP 352</div></div>
  <div><div class="slide-content"><img class="bottom-slider-img" src="img/amplifier-product-img1.png">MODEL NUMBER - MP 602</div></div>
  <div><div class="slide-content"><img class="bottom-slider-img" src="img/amplifier-product-img1.png">MODEL NUMBER - MP 452</div></div>
  <div><div class="slide-content"><img class="bottom-slider-img" src="img/amplifier-product-img1.png">MODEL NUMBER - MP 352</div></div>
</div>
</section>

<section id="contact">
  <div class="container footer-page-links">
    <div class="row">
      <div class="col-md-3">
        <h5 class="contact-sub-heading">CONTACT US</h5>

        <div class="addressholder">
        <ul>
        <li>
        <i><img class="img-fluid" src="img/location.png" alt="Contact"></i>
        <span class="ctnaddr">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</span>
        </li>
        <li>
        <i><img class="img-fluid" src="img/phone.png" alt="Contact"></i>
        <span class="ctnaddr">+91 98453-03199
        </span>
        </li>
        <li>
        <i><img class="img-fluid" src="img/mail.png" alt="Contact"></i>
        <span class="ctnaddr">prasad@awan.co.in</span>
        </li>
        </ul>
        </div>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">QUICK LINKS</h5>
        <ul>
              <li>
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">Products</a>
              </li>
              <li>
                <a href="#">Success Stories</a>
              </li>
              <li>
                <a href="#">Blog</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">EXTRAS</h5>
        <ul>
              <li>
                <a href="#">Downloads</a>
              </li>
              <li>
                <a href="#">News & Events</a>
              </li>
              <li>
                <a href="#">Privacy Policy</a>
              </li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="contact-sub-heading">NEWSLETTER</h5>
        <p>Give us your email and you will be daily updated with the latest events, in detail.</p>
        <div>
        <input class="news-input" type="email" name="email" placeholder="Enter Your Email Address"><img class="email-contact-img" src="img/newsletter-icon.png">
        </div>
        <h5 class="contact-sub-heading">FOLLOW US</h5>
        <p><img class="social-img" src="img/fb.png"><img class="social-img" src="img/twitter.png"><img class="social-img" src="img/whatsapp.png"><img class="social-img" src="img/insta.png"></p>

      </div>
    </div>
  </div>
  
</section>
<footer>
<div class="container copyright">
      <div class="row align-items-center">
        <div class="col-sm-3">
          <a href="index.php">
            <a href="#">Terms and Conditions</a>
          </a>
        </div>
        <div class="col-sm-6">
          © 2020. Magpie. All Rights Reserved.
        </div>
        <div class="col-sm-3">
          <a href="#">Powered by ViaMagus</a>
        </div>
      </div>
    </div>
</footer>

<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <h5 class="modal-title" id="exampleModalLabel">Get a Quote</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" required="" name="name" class="form-control" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email1" placeholder="Email">
          </div> 
          <div class="form-group">
            <input type="text" required="" name="subject" class="form-control" placeholder="Subject">
          </div>
          <div class="form-group">
          <textarea class="form-control" rows="5" id="comment" placeholder="Message"></textarea>
        </div>      
        </div>
        <div class="modal-footer border-top-0 d-flex justify-content-center">
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
 <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>
<script>
  $().ready(function(){
  $('.slick-carousel').slick({
    arrows: true,
    centerPadding: "0px",
    dots: false,
    slidesToShow: 3,
    infinite: false,
    easing:"linear",
     responsive:[
      {breakpoint:767,settings:{
        slidesToShow:2,
      }},
      {breakpoint:500,settings:{
        slidesToShow:1,
      }},
    ],
  });
});
</script>

</body>



</html>